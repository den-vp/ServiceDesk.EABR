﻿using servicedesk.frontend.Models.UserModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace servicedesk.frontend.Models.RequestModel
{
    public class RequestDto
    {
        public string Id { get; set; }
        [Display(Name = "Название заявки")]
        public string Name { get; set; }
        [Display(Name = "Описание заявки")]
        public string Description { get; set; }
        [Display(Name = "Коментарий к заявке")]
        public string Comment { get; set; }
        [Display(Name = "Статус заявки")]
        public int Status { get; set; }
        [Display(Name = "Приоритет заявки")]
        public int Priority { get; set; }
        [Display(Name = "Отдел сотрудника")]
        public string ActivId { get; set; }
        public string File { get; set; }
        [Display(Name = "Категория заявки")]
        public string ServiceId { get; set; }
        [Display(Name = "Исполнитель заявки")]
        public string ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }
        public string userId { get; set; }
        public string userCreated { get; set; }
        public string LifecycleId { get; set; }
        [Display(Name = "Дата открытия")]
        public DateTime Opened { get; set; }
        [Display(Name = "Дата распределения")]        
        public DateTime? Distributed { get; set; }
        [Display(Name = "Дата обработки")]
        public DateTime? Proccesing { get; set; }
        [Display(Name = "Дата проверки")]
        public DateTime? Checking { get; set; }
        [Display(Name = "Дата закрытия")]
        public DateTime? Closed { get; set; }
        public bool ExecuterFl { get; set; }
        public string ClientEmail { get; set; }
    }
    // Перечисление для статуса заявки
    public enum RequestStatus
    {
        [Display(Name = "Открыта")]
        Open = 0,
        [Display(Name = "Передана")]
        Distributed = 1,
        [Display(Name = "В обработке")]
        Proccesing = 2,
        [Display(Name = "Проверка")]
        Checking = 3,
        [Display(Name = "Закрыта")]
        Closed = 4
    }
    // Перечисление для приоритета заявки
    public enum RequestPriority
    {
        [Display(Name = "Низкий")]
        Low = 1,
        [Display(Name = "Средний")]
        Medium = 2,
        [Display(Name = "Высокий")]
        High = 3,
        [Display(Name = "Критический")]
        Critical = 4
    }

    public static class EnumDisplayExtensions
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            //определяем тип параметра и извлекаем массив его публичных членов по имени
            MemberInfo[] memberInfo = enumValue.GetType().GetMember(enumValue.ToString());
            //определяем тип класса атрибута
            Type attributeType = typeof(DisplayAttribute);
            //у первого публичного члена пытаемся получить массив
            //атрибутов типа DisplayAttribute
            object[] attributes = memberInfo[0].GetCustomAttributes(attributeType, false);
            //если поиск безуспешен или почему-то таких атрибутов найдено больше одного
            //вываливаем ошибку
            if (attributes == null || attributes.Length != 1)
                throw new ArgumentOutOfRangeException($"Невозможно найти атрибут по имени '{nameof(DisplayAttribute)}'");
            //хватаем атрибут
            var attribute = attributes.SingleOrDefault() as DisplayAttribute;
            //возвращаем значение его свойства по имени Name
            return attribute?.Name;
        }
    }
}

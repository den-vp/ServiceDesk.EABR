﻿namespace servicedesk.frontend.Service
{
    public class Config
    {
        public static string ConnectionString { get; set; }
        public static string CompanyName { get; set; }
        public static string CompanyPhone { get; set; }
        public static string CompanyPhoneShort { get; set; }
        public static string CompanyEmail { get; set; }
        public static string EabrApi { get; set; }
        public static string EabrIdentity { get; set; }
        public static string EabrClientSecret { get; set; }
    }
}

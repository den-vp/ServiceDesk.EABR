﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using servicedesk.frontend.Domain;
using servicedesk.frontend.Domain.Entities;
using servicedesk.frontend.Service;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace servicedesk.frontend.Areas.Admin.Controllers
{
    [Area("Admin"), Authorize(Roles = "admin")]
    public class ServiceItemsController : Controller
    {
        private readonly DataManager dataManager;
        private readonly IWebHostEnvironment hostingEnvironment;

        public ServiceItemsController(DataManager dataManager, IWebHostEnvironment hostingEnvironment)
        {
            this.dataManager = dataManager;
            this.hostingEnvironment = hostingEnvironment;
        }

        public async Task<ActionResult<string>> EditService(ServiceItem model)
        {
            try
            {
                var apiUrl = new Uri($"https://localhost:44328/api/1.0/Service");
                using (var apiClient = new HttpClient())
                {
                    var accessToken = await HttpContext.GetTokenAsync("access_token");
                    apiClient.SetBearerToken(accessToken);
                    HttpResponseMessage response;
                    if (Guid.Empty == model.Id)
                    {
                        response = await apiClient.PostAsJsonAsync(apiUrl, new { name = model.Title, subtitle = model.Subtitle });
                    }
                    else
                    {
                        response = await apiClient.PutAsJsonAsync(apiUrl, new { id = model.Id, name = model.Title, subtitle = model.Subtitle });
                    }                   
                    response.EnsureSuccessStatusCode();

                    return response.Headers.Location.ToString();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { code = ex.ToString() });
            }
        }
        public IActionResult Edit(Guid id)
        {
            var entity = id == default ? new ServiceItem() : dataManager.ServiceItems.GetServiceItemById(id);
            return View(entity);
        }
        [HttpPost]
        public IActionResult Edit(ServiceItem model, IFormFile titleImageFile)
        {
            if (ModelState.IsValid)
            {
                if (titleImageFile != null)
                {
                    model.TitleImagePath = titleImageFile.FileName;
                    using (var stream = new FileStream(Path.Combine(hostingEnvironment.WebRootPath, "images/", titleImageFile.FileName), FileMode.Create))
                    {
                        titleImageFile.CopyTo(stream);
                    }
                }                
                dataManager.ServiceItems.SaveServiceItem(model);
                return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).CutController());
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            dataManager.ServiceItems.DeleteServiceItem(id);
            return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).CutController());
        }
    }
}

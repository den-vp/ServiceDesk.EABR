﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ServiceDesk.Identity.Models
{
    public class ChangeRoleViewModel1
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        public List<IdentityRole> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public ChangeRoleViewModel1()
        {
            AllRoles = new List<IdentityRole>();
            UserRoles = new List<string>();
        }

    }
}

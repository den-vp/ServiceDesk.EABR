﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Domain.Entities
{
    public class Request : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public virtual Activ Activ { get; set; }
        public string File { get; set; }
        public Guid? ServiceId { get; set; }
        public Guid UserId { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }

        public Guid LifecycleId { get; set; }
        public virtual Lifecycle Lifecycle { get; set; }        
        public string ClientEmail { get; set; }
    }
    // Перечисление для статуса заявки
    public enum RequestStatus
    {
        Open = 0,
        Distributed = 1,
        Proccesing = 2,
        Checking = 3,
        Closed = 4
    }
    // Перечисление для приоритета заявки
    public enum RequestPriority
    {
        Low = 1,
        Medium = 2,
        High = 3,
        Critical = 4
    }
}

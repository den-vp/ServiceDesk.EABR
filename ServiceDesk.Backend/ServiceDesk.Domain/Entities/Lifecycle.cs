﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Domain.Entities
{
    public class Lifecycle : EntityBase
    {
        // Дата открытия
        public DateTime Opened { get; set; }
        // Дата распределения
        public DateTime? Distributed { get; set; }
        // Дата обработки
        public DateTime? Proccesing { get; set; }
        // Дата проверки
        public DateTime? Checking { get; set; }
        // Дата закрытия
        public DateTime? Closed { get; set; }
    }
}

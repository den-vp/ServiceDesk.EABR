﻿using System;

namespace ServiceDesk.Domain.Entities
{
    public class Activ : EntityBase
    {
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.CreateActiv;
using ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.DeleteActiv;
using ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.UpdateActiv;
using ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivDetails;
using ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivList;
using ServiceDesk.WebApi.Models.ActivModel;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ServiceDesk.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class ActivController : BaseController
    {
        private readonly IMapper _mapper;

        public ActivController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets the list of activs
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /activ
        /// </remarks>
        /// <param name="departName">CabNumber</param>
        /// <returns>Returns ActivListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ActivListVm>> GetAllDepartment(string departName)
        {
            var query = new GetActivListQuery
            {
                DepartmentName = departName
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Gets the activ by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /activ/D34D349E-43B8-429E-BCA4-793C932FD580
        /// </remarks>
        /// <param name="id">Activ id (guid)</param>
        /// <returns>Returns ActivDetailsVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user in unauthorized</response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ActivDetailsVm>> Get(Guid id)
        {
            var query = new GetActivDetailsQuery
            {
                Id = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Creates the activ
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /activ
        /// {
        ///     CabNumber: "CabNumber",
        ///     DepartmentId: "ID Department"
        /// }
        /// </remarks>
        /// <param name="createActivDto">CreateDepartmentDto object</param>
        /// <returns>Returns id (guid)</returns>
        /// <response code="201">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Guid>> Create([FromBody] CreateActivDto createActivDto)
        {
            var command = _mapper.Map<CreateActivCommand>(createActivDto);
            command.CabNumber = createActivDto.CabNumber;
            command.DepartmentId = createActivDto.DepartmentId;

            var actId = await Mediator.Send(command);
            return Ok(actId);
        }

        /// <summary>
        /// Updates the department
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// PUT /activ
        /// {
        ///     CabNumber: "updated CabNumber",
        ///     DepartmentId: "updated DepartmentId",
        /// }
        /// </remarks>
        /// <param name="updateActivDto">UpdateActivDto object</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] UpdateActivDto updateActivDto)
        {
            var command = _mapper.Map<UpdateActivCommand>(updateActivDto);
            command.CabNumber = updateActivDto.CabNumber;
            command.DepartmentId = updateActivDto.DepartmentId;
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Deletes the department by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /activ/88DEB432-062F-43DE-8DCD-8B6EF79073D3
        /// </remarks>
        /// <param name="id">Id of the Activ (guid)</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteActivCommand
            {
                Id = id,
            };
            await Mediator.Send(command);
            return NoContent();
        }

    }
}

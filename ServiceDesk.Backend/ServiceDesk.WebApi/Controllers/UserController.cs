﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserDetails;
using ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserList;
using ServiceDesk.WebApi.Models.ActivModel;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;


namespace ServiceDesk.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class UserController : BaseController
    {
        private readonly IMapper _mapper;

        public UserController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets the list of user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /user
        /// </remarks>
        /// <param name="userName">CabNumber</param>
        /// <returns>Returns UserListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<UserListVm>> GetAllUser(string userName)
        {
            var query = new GetUserListQuery
            {
                Name = userName
            };
            var vm = await Mediator.Send(query);

            return Ok(vm);
        }
        /// <summary>
        /// Gets the user by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /user/D34D349E-43B8-429E-BCA4-793C932FD580
        /// </remarks>
        /// <param name="id">User id (guid)</param>
        /// <returns>Returns UserDetailsVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user in unauthorized</response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<UserDetailsVm>> Get(string id)
        {
            var query = new GetUserDetailsQuery
            {
                Id = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }


    }
}

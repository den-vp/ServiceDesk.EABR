﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.CreateDepartment;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.WebApi.Models.DepartmentModel
{
    public class CreateDepartmentDto : IMapWith<CreateDepartmentCommand>
    {
        [Required]
        public string Name { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CreateDepartmentDto, CreateDepartmentCommand>()
                .ForMember(depCommand => depCommand.Name,
                    opt => opt.MapFrom(depDto => depDto.Name));
        }
    }
}

﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.UpdateActiv;
using System;

namespace ServiceDesk.WebApi.Models.ActivModel
{
    public class UpdateActivDto : IMapWith<UpdateActivCommand>
    {
        public Guid Id { get; set; }
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateActivDto, UpdateActivCommand>()
                .ForMember(actCommand => actCommand.Id,
                    opt => opt.MapFrom(actDto => actDto.Id))
                .ForMember(actCommand => actCommand.CabNumber,
                    opt => opt.MapFrom(actDto => actDto.CabNumber))
                .ForMember(actCommand => actCommand.DepartmentId,
                    opt => opt.MapFrom(actDto => actDto.DepartmentId));
        }
    }
}

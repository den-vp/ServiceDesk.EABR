﻿using ServiceDesk.Persistence;
using System;

namespace Departments.Tests.Common
{
    public class TestCommandBase : IDisposable
    {
        protected readonly ServiceDeskDbContext Context;

        public TestCommandBase()
        {
            Context = DepartmentsContextFactory.Create();
        }

        public void Dispose()
        {
            DepartmentsContextFactory.Destroy(Context);
        }
    }
}

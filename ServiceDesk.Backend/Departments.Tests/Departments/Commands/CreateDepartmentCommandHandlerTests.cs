﻿using Departments.Tests.Common;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.CreateDepartment;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Departments.Tests.Departments.Commands
{
    public class CreateDepartmentCommandHandlerTests : TestCommandBase
    {
        [Fact]
        public async Task CreateNoteCommandHandler_Success()
        {
            // Arrange
            var handler = new CreateDepartmentCommandHandler(Context);
            var noteName = "department name";

            // Act
            var depId = await handler.Handle(
                new CreateDepartmentCommand
                {
                    Name = noteName
                },
                CancellationToken.None);

            // Assert
            Assert.NotNull(
                await Context.Departments.SingleOrDefaultAsync(dep =>
                    dep.Id == depId && dep.Name == noteName ));
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using ServiceDesk.Domain.Entities;

namespace ServiceDesk.Application.Interfaces
{
    public interface IServiceDeskDbContextUser
    {
        public DbSet<AppUser> Users { get; set; }
    }
}

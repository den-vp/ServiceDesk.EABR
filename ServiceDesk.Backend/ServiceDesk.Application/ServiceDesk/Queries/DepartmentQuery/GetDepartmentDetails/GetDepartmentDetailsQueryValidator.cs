﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentDetails
{
    public class GetDepartmentDetailsQueryValidator : AbstractValidator<GetDepartmentDetailsQuery>
    {
        public GetDepartmentDetailsQueryValidator()
        {
            RuleFor(dep => dep.Id).NotEqual(Guid.Empty);
        }
    }
}

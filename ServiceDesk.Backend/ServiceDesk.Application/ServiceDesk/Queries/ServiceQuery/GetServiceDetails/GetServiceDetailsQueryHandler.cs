﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;


namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceDetails
{
    public class GetServiceDetailsQueryHandler
    : IRequestHandler<GetServiceDetailsQuery, ServiceDetailsVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetServiceDetailsQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) => (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<ServiceDetailsVm> Handle(GetServiceDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Services
                .FirstOrDefaultAsync(dep =>
                dep.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Service), request.Id);
            }

            return _mapper.Map<ServiceDetailsVm>(entity);
        }
    }
}

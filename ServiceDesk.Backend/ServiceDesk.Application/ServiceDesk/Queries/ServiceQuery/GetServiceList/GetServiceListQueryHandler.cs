﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceList
{
    public class GetServiceListQueryHandler : IRequestHandler<GetServiceListQuery, ServiceListVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetServiceListQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<ServiceListVm> Handle(GetServiceListQuery request,
            CancellationToken cancellationToken)
        {            
            var serQuery = await _dbContext.Services
                .Where(ser => String.IsNullOrEmpty(request.Name) || ser.Name.StartsWith(request.Name))
                .ProjectTo<ServiceLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            return new ServiceListVm { Services = serQuery };
        }
    }
}

﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserDetails
{
    public class UserDetailsVm : IMapWith<AppUser>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AppUser, UserDetailsVm>()
                .ForMember(usrVm => usrVm.Id,
                    opt => opt.MapFrom(usr => usr.Id))
                .ForMember(usrVm => usrVm.Name,
                    opt => opt.MapFrom(usr => $"{usr.FirstName} {usr.LastName}"))
                .ForMember(usrVm => usrVm.Login,
                    opt => opt.MapFrom(usr => usr.UserName))
                .ForMember(usrVm => usrVm.Email,
                    opt => opt.MapFrom(usr => usr.Email));
        }
    }

}

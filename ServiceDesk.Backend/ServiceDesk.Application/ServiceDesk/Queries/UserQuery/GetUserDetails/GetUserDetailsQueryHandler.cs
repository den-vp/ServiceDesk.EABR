﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserDetails
{
    public class GetUserDetailsQueryHandler
     : IRequestHandler<GetUserDetailsQuery, UserDetailsVm>
    {
        private readonly IServiceDeskDbContextUser _dbContext;
        private readonly IMapper _mapper;

        public GetUserDetailsQueryHandler(IServiceDeskDbContextUser dbContext,
            IMapper mapper) => (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<UserDetailsVm> Handle(GetUserDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Users
                .FirstOrDefaultAsync(usr =>
                usr.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(AppUser), request.Id);
            }

            return _mapper.Map<UserDetailsVm>(entity);
        }
    }
}

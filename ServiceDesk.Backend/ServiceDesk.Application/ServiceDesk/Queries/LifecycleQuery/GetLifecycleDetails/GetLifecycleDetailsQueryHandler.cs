﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleDetails
{
    public class GetLifecycleDetailsQueryHandler : IRequestHandler<GetLifecycleDetailsQuery, LifecycleDetailsVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetLifecycleDetailsQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) => (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<LifecycleDetailsVm> Handle(GetLifecycleDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Lifecycles
                .FirstOrDefaultAsync(dep =>
                dep.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Lifecycle), request.Id);
            }

            return _mapper.Map<LifecycleDetailsVm>(entity);
        }
    }
}

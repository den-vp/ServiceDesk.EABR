﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleList
{
    public class GetLifecycleListQueryValidator : AbstractValidator<GetLifecycleListQuery>
    {
        public GetLifecycleListQueryValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Id).NotNull();
        }
    }
}

﻿using System.Collections.Generic;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleList
{
    public class LifecycleListVm
    {
        public IList<LifecycleLookupDto> Lifecycles { get; set; }
    }
}

﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivDetails
{
    public class GetActivDetailsQuery : IRequest<ActivDetailsVm>
    {
        public Guid Id { get; set; }
    }
}

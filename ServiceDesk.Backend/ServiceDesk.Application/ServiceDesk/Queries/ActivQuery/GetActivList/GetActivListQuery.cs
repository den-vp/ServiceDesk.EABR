﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivList
{
    public class GetActivListQuery : IRequest<ActivListVm>
    {
        public string CabNumber { get; set; }
        public string DepartmentName { get; set; }
        public Guid DepartmentId { get; set; }
    }
}

﻿using MediatR;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.CreateLifecycle
{
    public class CreateLifecycleCommandHandler : IRequestHandler<CreateLifecycleCommand, Guid>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public CreateLifecycleCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Guid> Handle(CreateLifecycleCommand request,
            CancellationToken cancellationToken)
        {
            var lifecycle = new Lifecycle
            {
                Id = Guid.NewGuid(),
                Opened = DateTime.Now,
                Distributed = request.Distributed,
                Proccesing = request.Proccesing,
                Checking = request.Checking,
                Closed = null
            };

            await _dbContext.Lifecycles.AddAsync(lifecycle, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return lifecycle.Id;
        }
    }
}

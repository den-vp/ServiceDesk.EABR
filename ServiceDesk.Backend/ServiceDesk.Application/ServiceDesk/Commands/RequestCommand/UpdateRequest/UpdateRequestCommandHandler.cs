﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.UpdateRequest
{
    public class UpdateRequestCommandHandler
        : IRequestHandler<UpdateRequestCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public UpdateRequestCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(UpdateRequestCommand request,
            CancellationToken cancellationToken)
        {
            var entity =
                await _dbContext.Requests
                .Include(req => req.Lifecycle)
                .FirstOrDefaultAsync(req =>
                    req.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Request), request.Id);
            }

            entity.Name = request.Name;
            entity.Description = request.Description;
            entity.Comment = request.Comment;
            entity.Status = request.Status;
            entity.Priority = request.Priority;
            entity.ActivId = request.ActivId;
            entity.File = request.File;
            entity.ServiceId = request.ServiceId;
            entity.UserId = request.UserId;
            entity.ExecutorId = request.ExecutorId;
            entity.ExecutorLogin = request.ExecutorLogin;
            entity.ExecutorName = request.ExecutorName;
            entity.ExecutorEmail = request.ExecutorEmail;
            entity.Lifecycle.Proccesing = request.Proccesing;
            entity.Lifecycle.Distributed = request.Distributed;
            entity.Lifecycle.Checking = request.Checking;

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}

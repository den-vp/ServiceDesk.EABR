﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.DeleteService
{
    public class DeleteServiceCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}

﻿using MediatR;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.CreateDepartment
{
    public class CreateDepartmentCommandHandler 
        : IRequestHandler<CreateDepartmentCommand, Guid>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public CreateDepartmentCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Guid> Handle(CreateDepartmentCommand request,
            CancellationToken cancellationToken)
        {
            var department = new Department
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                CreationDate = DateTime.Now,
                EditDate = null
            };

            await _dbContext.Departments.AddAsync(department, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return department.Id;
        }
    }
}

﻿using System;
using FluentValidation;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.CreateActiv
{
    public class CreateActivCommandValidator : AbstractValidator<CreateActivCommand>
    {
        public CreateActivCommandValidator()
        {
            RuleFor(createActivCommand => createActivCommand.CabNumber).NotEmpty().MaximumLength(10).WithMessage("Превышена максимальная длинна кабинета");
            RuleFor(createActivCommand => createActivCommand.DepartmentId).NotEqual(Guid.Empty).WithMessage("Департамент не может быть пустым");
        }
    }
}

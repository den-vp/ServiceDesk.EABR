﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.CreateActiv
{
    public class CreateActivCommandHandler : IRequestHandler<CreateActivCommand, Guid>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public CreateActivCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Guid> Handle(CreateActivCommand request,
            CancellationToken cancellationToken)
        {
            var activ = new Activ
            {
                Id = Guid.NewGuid(),
                CabNumber = request.CabNumber,
                //Department = entity,
                DepartmentId = request.DepartmentId,
                CreationDate = DateTime.Now,
                EditDate = null
            };

            await _dbContext.Activs.AddAsync(activ, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return activ.Id;
        }
    }
}
